# Docker container for runnning Spark jobs on a Mesos cluster

See [there](http://clouddocs.web.cern.ch/clouddocs/containers/mesos.html) for creating a Mesos cluster

Run a Spark shell using using this command
```bash
sudo docker run --rm -it --net=host \
  gitlab-registry.cern.ch/benoel/spark-on-mesos spark-shell \
  --master mesos://zk://$MESOS_MASTER_IP:2181/mesos \
  --conf spark.mesos.executor.docker.image=gitlab-registry.cern.ch/benoel/spark-on-mesos
```
The `spark.mesos.executor.docker.image` parameter is used to tell Mesos workers which container image to create the Spark workers.

Note that the container will be contacted by the Mesos cluster (the Mesos masters and workers will open connections from their side to your container). That is why you have to run your container in the network of the host (`--net host`). And it means you have to check that your firewall accepts incoming connections. If the container is blocked at `No credentials provided. Attempting to register without authentication`, it means Mesos could not contact you.

Here is a simple code to test that everything works fine
```java
val NUM_SAMPLES = 1000
val count = sc.parallelize(1 to NUM_SAMPLES).map{i =>
  val x = Math.random()
  val y = Math.random()
  if (x*x + y*y < 1) 1 else 0
}.reduce(_ + _)
println("Pi is roughly " + 4.0 * count / NUM_SAMPLES)
```


The container has all the Spark binaries, so you can also submit a job, using
```bash
SPARK_JAR_PATH=...
sudo docker run --rm -it --net=host \
  -v $SPARK_JAR_PATH:/work:z \
  gitlab-registry.cern.ch/benoel/spark-on-mesos spark-submit \
  --master mesos://zk://$MESOS_MASTER_IP:2181/mesos \
  --conf spark.mesos.executor.docker.image=gitlab-registry.cern.ch/benoel/spark-on-mesos \
  --num-executors 2 \
  --driver-memory 1g \
  --executor-memory 1g \
  --class org.apache.spark.examples.SparkPi \
  /work/spark-examples_2.11-2.0.0.jar 1000
```

After starting your container, you should see a new framework appearing in your Mesos UI (http://$MESOS_MASTER:5050/#/frameworks). Spark shell has a monitoring webpage on the machine you launch the client http://$MY_IP:4040/

Note that you might have to wait before launching any processing for the first time, because the Mesos workers are downloading your Docker image.

If your Mesos cluster is shared, you might want to run using the coarse parameter.
